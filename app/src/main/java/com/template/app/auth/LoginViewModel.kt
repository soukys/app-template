package com.template.app.auth

import android.annotation.SuppressLint
import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.google.firebase.auth.FirebaseAuth
import com.template.app.AppBase
import com.template.app.system.api.Api
import com.template.app.system.api.ApiEvent
import com.template.app.system.api.ApiException
import com.template.app.system.api.responses.LoginResponse
import com.template.app.user.UserStorage
import com.template.app.utils.SingleLiveEvent
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


@SuppressLint("CheckResult")
class LoginViewModel : ViewModel() {

    @Inject
    lateinit var api: Api

    @Inject
    lateinit var userStorage: UserStorage

    val loggedInEvent = SingleLiveEvent<ApiEvent>()
    val loading = ObservableField(false)

    init {
        AppBase.app.appComponent.inject(this)

    }

    fun login(email: String, password: String) {
        loading.set(true)
        loginGeneric(api.login(email, password))
    }

    private fun loginGeneric(action: Observable<LoginResponse>) {
        action.subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                it.requireCode(1)
                userStorage.storeUser(it.user)
                userStorage.accessToken = it.user.accessToken
                it.user.accessToken = null
                loggedInEvent.value = ApiEvent(it.code, true, null)
                loading.set(false)
            }, {
                if (it is ApiException) {
                    loggedInEvent.value = ApiEvent(it.code, false, it)
                } else {
                    loggedInEvent.value = ApiEvent(null, false, it)
                }
                it.printStackTrace()
                loading.set(false)
            })
    }
}