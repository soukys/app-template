package com.template.app.auth.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.Observable
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.template.app.MainActivity
import com.template.app.R
import com.template.app.auth.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import com.google.firebase.auth.FirebaseAuth
import com.template.app.user.UserStorage
import com.template.app.user.model.User
import javax.inject.Inject


class LoginActivity : AppCompatActivity() {
    lateinit var loginViewModel: LoginViewModel
    private lateinit var auth: FirebaseAuth

    lateinit var userStorage: UserStorage

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loginViewModel = ViewModelProviders.of(this)[LoginViewModel::class.java]
        auth = FirebaseAuth.getInstance()

        setContentView(R.layout.activity_login)

        btnLogin.setOnClickListener {
            when {
                etEmail.text?.isEmpty()!! -> Toast.makeText(
                    this,
                    getString(R.string.err_missing_email_alert),
                    Toast.LENGTH_SHORT
                ).show()
                etPassword.text?.isEmpty()!! -> Toast.makeText(
                    this,
                    getString(R.string.err_password_missing_alert),
                    Toast.LENGTH_SHORT
                ).show()
                etPassword.text?.length!! < 6 -> Toast.makeText(
                    this,
                    getString(R.string.err_password_len_alert),
                    Toast.LENGTH_SHORT
                ).show()
                else -> {
                    pbLoading.visibility = View.VISIBLE
//                    loginViewModel.login(
//                        etEmail.text.toString(),
//                        etPassword.text.toString()
//                    )
                    signIn(etEmail.text.toString(), etPassword.text.toString())
                }
            }
        }

        btnRegister.setOnClickListener {
            when {
                etEmail.text?.isEmpty()!! -> Toast.makeText(
                    this,
                    getString(R.string.err_missing_email_alert),
                    Toast.LENGTH_SHORT
                ).show()
                etPassword.text?.isEmpty()!! -> Toast.makeText(
                    this,
                    getString(R.string.err_password_missing_alert),
                    Toast.LENGTH_SHORT
                ).show()
                etPassword.text?.length!! < 6 -> Toast.makeText(
                    this,
                    getString(R.string.err_password_len_alert),
                    Toast.LENGTH_SHORT
                ).show()
                else -> {
                    pbLoading.visibility = View.VISIBLE
//                    loginViewModel.register(
//                        etEmail.text.toString(),
//                        etPassword.text.toString()
//                    )
                    register(etEmail.text.toString(), etPassword.text.toString())
                }
            }
        }

        loginViewModel.loading.addOnPropertyChanged { pbLoading.visibility = View.GONE }

        loginViewModel.loggedInEvent.observe(this, Observer {
            pbLoading.visibility = View.GONE
            if (it!!.success) {
                startActivity<MainActivity>()
                finish()
            } else {
                if (it.code == 101) {
                    toast(getString(R.string.err_wrong_email))
                } else if (it.code == 110) {
                    toast(getString(R.string.err_invalid_mail_or_password))
                } else if (it.code == 102) {
                    toast(getString(R.string.err_account_already_exists))
                } else {
                    toast(getString(R.string.err_network_connectivity_error))
                }
            }
        })
    }

    fun <T : Observable> T.addOnPropertyChanged(callback: (T) -> Unit) =
        addOnPropertyChangedCallback(
            object : Observable.OnPropertyChangedCallback() {
                override fun onPropertyChanged(
                    observable: Observable?, i: Int
                ) =
                    callback(observable as T)
            })


    ///////////////////////////////// FIREBASE LOGIN ///////////////////////////
    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if (currentUser != null) {
            startActivity<MainActivity>()
            finish()
        }
    }

    fun register(email: String, password: String) {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    val user = auth.currentUser
                    userStorage.user = User(0, user?.displayName!!, email, true, user.getIdToken(true).toString())
                    startActivity<MainActivity>()
                    finish()
                } else {
                    // If sign in fails, display a message to the user.
                    Toast.makeText(
                        baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                pbLoading.visibility = View.GONE
            }
    }

    private fun signIn(email: String, password: String) {
        pbLoading.visibility = View.VISIBLE
        // [START sign_in_with_email]
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    startActivity<MainActivity>()
                    finish()
                } else {
                    // If sign in fails, display a message to the user.
                    Toast.makeText(
                        baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                pbLoading.visibility = View.GONE
            }
    }

    private fun signOut() {
        auth.signOut()
    }
}