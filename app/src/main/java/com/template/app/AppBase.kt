package com.template.app

import android.app.Application
import com.template.app.system.dagger.ApiModule
import com.template.app.system.dagger.AppComponent
import com.template.app.system.dagger.DaggerAppComponent

class AppBase : Application() {
    val appComponent : AppComponent by lazy {
        DaggerAppComponent
            .builder()
            .apiModule(ApiModule())
            .build()
    }

    override fun onCreate() {
        super.onCreate()

        app = this
    }

    companion object {
        lateinit var app: AppBase
    }
}