package com.template.app.system.api.responses

import com.template.app.user.model.User


class LoginResponse(
    val accessToken: String,
    val user: User
) : Response()