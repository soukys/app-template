package com.template.app.system.api.responses

import com.google.gson.annotations.SerializedName
import com.template.app.system.api.ApiException

open class Response {
    @SerializedName("status")
    var code: Int = -100
    var message: String? = null

    fun requireCode(requiredCode: Int) {
        if(requiredCode != code) {
            throw ApiException(code)
        }
    }

    fun requireSuccess() {
        if(code < 0) {
            throw ApiException(code)
        }
    }
}