package com.template.app.system.dagger

import android.content.Context
import android.content.SharedPreferences
import com.template.app.AppBase
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class StorageModule {
    @Provides
    @Singleton
    @Named("user_prefs")
    fun provideUserPrefs(): SharedPreferences =
        AppBase.app.getSharedPreferences("user", Context.MODE_PRIVATE)

}