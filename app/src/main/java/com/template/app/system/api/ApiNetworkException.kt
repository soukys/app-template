package com.template.app.system.api

class ApiNetworkException() : Exception() {
    override val message: String
        get() = "Failed to connect to the server"
}