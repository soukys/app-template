package com.template.app.system.api

import com.template.app.system.api.responses.LoginResponse
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface Api {
    @FormUrlEncoded
    @POST("login")
    fun login(@Field("email") email: String, @Field("password") password: String): Observable<LoginResponse>

    @FormUrlEncoded
    @POST("register")
    fun register(@Field("email") email: String, @Field("password") password: String, @Field("name") name: String): Observable<LoginResponse>

}