package com.template.app.system.api

data class ApiEvent (
        val code: Int?,
        val success: Boolean,
        val error: Throwable?
)