package com.template.app.system.api

class ApiException(val code: Int) : Exception() {
    override val message: String
        get() = "Invalid API code received: [$code] ${messages[code]}"

    companion object {
        val SUCCESS = 1

        val messages = hashMapOf(
                SUCCESS to "Success"
        )
    }
}