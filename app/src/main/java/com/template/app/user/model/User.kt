package com.template.app.user.model

data class User(
    val id: Long,
    var name: String,
    var email: String,
    var hasPassword: Boolean,
    var accessToken: String?
)