package com.template.app.user

import android.content.SharedPreferences
import com.google.gson.Gson
import com.template.app.user.model.User
import javax.inject.Inject
import javax.inject.Named

class UserStorage @Inject constructor() {

    @Inject
    lateinit var gson: Gson

    @Inject
    @field:Named("user_prefs")
    lateinit var prefs: SharedPreferences

    var accessToken: String?
        get() = prefs.getString("access_token", null)
        set(value) = prefs.edit().putString("access_token", value).apply()

    var user: User?
        get() = loadUser()
        set(value) = storeUser(value!!)

    fun storeUser(user: User) {
        prefs.edit().putString("user", gson.toJson(user)).apply()
    }

    fun loadUser(): User? {
        val json = prefs.getString("user", null) ?: return null
        return gson.fromJson(json, User::class.java)
    }

    fun clearUserData() {
        prefs.edit().clear().apply()
    }

    fun logout() {
        clearUserData()
    }
}