# How to use this Android/Kotlin app pipeline template?

1) clone this repository

* Change package name (ie. com.wildbear.app) in AndroidManifest.xml, build.gradle and project's classes

2) Setup your own API 

* provide your real google-services.json (used for mocking of auth) for google && firebase depencies

3) use gitflow https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow

4) accordingly to gitflow branching: 

* **develop** and **feature/<new-awesome-feature>** (almost always)

* **release** for production candidates

* **master** branch for production app 

5) don't forget to do manual **semantic apk versioning** (major.minor.patch) through variables in gradle.build file

6) You're done :) !

## Environment of used docker image https://hub.docker.com/r/mingc/android-build-box

* Ubuntu 18.04

* Android SDK 16 17 18 19 20 21 22 23 24 25 26 27 28

* Android build tools:

17.0.0,

18.1.1,

19.1.0,

20.0.0,

21.1.2 22.0.1,

23.0.1 23.0.2 23.0.3,

24.0.0 24.0.1 24.0.2 24.0.3,

25.0.0 25.0.1 25.0.2 25.0.3,

26.0.0 26.0.1 26.0.2,

27.0.1 27.0.2 27.0.3,

28.0.1 28.0.2 28.0.3

* Android NDK r20

* extra-android-m2repository

* extra-google-m2repository

* extra-google-google_play_services

* Google API add-ons

* Android Emulator

* Constraint Layout

* TestNG

* Python 2, Python 3

* Node.js, npm, React Native

* Ruby, RubyGems

* fastlane

* Kotlin 1.3

* Flutter 1.5.4
